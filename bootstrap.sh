#!/bin/sh
# bootstrap.sh glue
set -e

check_program () {
    if [ -z "`which $1 2>/dev/null`" ]; then
        echo "Could not find $1 program: aptitude install $2"
        exit 1
    fi
}

check_program automake automake
check_program autoreconf autoconf
check_program libtool libtool
check_program pkg-config pkg-config

echo "Bootstrapping system..."
export AUTOMAKE="automake --foreign -a"
autoreconf -f -i -s

exit 0
