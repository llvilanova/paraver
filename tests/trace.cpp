/*
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <string>
#include <stdio.h>

#define CHECKPKG "trace"
#include "checks.h"
#include <paratrace/Trace.h>


static
void
fill_trace (paratrace::Trace * trace)
{
    paratrace::clock time = 100;
    paratrace::Context c1(1,1,1);
    paratrace::Context c2(1,1,2);

    // trace->setStart(time);
    trace->registerNode(2);

    time++;

    trace->registerState(0, "none", paratrace::Color::black);
    trace->registerState(1, "running", paratrace::Color::green);
    trace->registerState(2, "waiting", paratrace::Color::red);
    trace->registerState(3, "sending", paratrace::Color::blue);
    trace->registerState(4, "receiving", paratrace::Color::light_blue);
    trace->registerEvent(0, "migration");

    // both start running
    trace->recordStateBegin(0, c1, time, 1);
    trace->recordStateBegin(1, c2, time, 1);

    time += 10;

    // c2 waits for receive
    trace->recordStateEnd(c2, time);
    trace->recordStateBegin(1, c2, time, 2);

    time += 10;

    // c1 starts sending
    trace->recordStateEnd(c1, time);
    trace->recordStateBegin(0, c1, time, 3);
    trace->recordCommLogBegin(0, 100, 0, c1, time);

    time += 5;

    // c2 starts receiving
    trace->recordCommPhysBegin(0, time);
    trace->recordStateEnd(c2, time);
    trace->recordStateBegin(1, c2, time, 4);

    time += 45;

    // c1 finishes sending
    trace->recordCommPhysEnd(0, time);
    trace->recordStateEnd(c1, time);
    trace->recordStateBegin(0, c1, time, 1);

    time += 5;

    // c2 finishes receiving
    trace->recordCommLogEnd(0, 1, c2, time);
    trace->recordStateEnd(c2, time);
    trace->recordStateBegin(1, c2, time, 1);

    time += 10;

    // c2 ends
    trace->recordStateEnd(c2, time);

    time += 5;

    // c1 migrates
    trace->recordStateEnd(c1, time);
    trace->recordEvent(0, c1, time, 0, 100);
    trace->recordStateBegin(1, c1, time, 1);

    time += 5;

    // c1 ends
    trace->recordStateEnd(c1, time);
}

static
void
compare_file (const std::string sout, const std::string sref,
              const bool prv = false)
{
    FILE * out = fopen(sout.c_str(), "r");
    CSERROR(out, "could not open output file %s", sout.c_str());
    FILE * ref = fopen(sref.c_str(), "r");
    CSERROR(ref, "could not open reference file %s", sref.c_str());

    if (prv) {
        // skip past the timestamp header
        fseek(out, 30, SEEK_SET);
        fseek(ref, 30, SEEK_SET);
    }

    char * buf[2][4096];
    size_t ototal = 0, rtotal = 0;
    size_t oread, rread, min;

    do {
        oread = fread(buf[0], 1, 4096, out);
        rread = fread(buf[1], 1, 4096, ref);

        min = oread < rread ? oread : rread;

        if (memcmp(buf[0], buf[1], min) != 0) {
            ERROR("output and reference files %s and %s differ",
                  sout.c_str(), sref.c_str());
        }

        ototal += oread;
        rtotal += rread;

        CERROR(ototal == rtotal,
               "output and reference files %s and %s differ",
               sout.c_str(), sref.c_str());
    } while (oread && rread);
}

int
main (int argc, char * argv[])
{
    paratrace::Trace * trace = new paratrace::Trace("trace-out");

    fill_trace(trace);
    delete trace;
    compare_file("trace-out.prv", "trace-ref.prv", true);
    compare_file("trace-out.pcf", "trace-ref.pcf");
    compare_file("trace-out.row", "trace-ref.row");
    return 0;
}
