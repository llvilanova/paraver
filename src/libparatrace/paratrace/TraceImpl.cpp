/*
 * Copyright (C) 2010, 2011 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <string>

#define CHECKPKG "libparatrace"
#include "checks.h"
#include "formats.h"

#include "paratrace.h"
#include "paratrace/Trace.h"
#include "paratrace/TraceImpl.h"
#include "paratrace/Record/Event.h"
#include "paratrace/Record/State.h"
#include "paratrace/Record/Comm.h"


#define PRV_MAX_HEADER 500


static
const std::string
path_strip_suffix (const std::string path)
{
    const std::string suffix = ".prv";
    size_t base_len = path.length() - suffix.length();

    if (path.length() >= suffix.length() && path.substr(base_len) == suffix) {
        return path.substr(0, base_len);
    }
    return path;
}


paratrace::TraceImpl::TraceImpl (const std::string path)
    :_clock_start(0)
    ,_clock_stop(0)
{
    const std::string base = path_strip_suffix(path);

    _prv = fopen((base + ".prv").c_str(), "w");
    CSFATAL(_prv, "creating .prv file");
    _pcf = fopen((base + ".pcf").c_str(), "w");
    CSFATAL(_pcf, "creating .pcf file");
    _row = fopen((base + ".row").c_str(), "w");
    CSFATAL(_row, "creating .row file");

    // This is really ugly, but hopefully reserves enough space for the paraver
    // header to fit, without having to dump the whole trace to a temporal file
    // at the end.
    for (int i = 0; i < PRV_MAX_HEADER; i++)
        fprintf(_prv, " ");
    fprintf(_prv, "\n");
}


#define wprv(o...) do { fprintf(_prv, ##o); } while (0)
#define wpcf(o...) do { fprintf(_pcf, ##o); } while (0)
#define wrow(o...) do { fprintf(_row, ##o); } while (0)

void
paratrace::TraceImpl::writePrvHeader ()
{
    // Fill-in .prv header
    rewind(_prv);

    time_t tt = time(NULL);
    CSFATAL(tt != ((time_t)-1), "time");
    struct tm * t = localtime(&tt);
    CSFATAL(t != NULL, "localtime");

    wprv("#Paraver (%02d/%02d/%04d at %02d:%02d):",
         t->tm_mday, t->tm_mon, t->tm_year + 1900,
         t->tm_hour, t->tm_min);

    // Trace duration
    wprv(FMT_clock, _clock_stop);

    _resources.writePrvHeader(_prv);
    _contexts.writePrvHeader(_prv);

    /** @todo Circumvents the "invalid number of communicators" error */
    wprv(",0");

    /** @todo Should fall back to a temporal copy when this fails. */
    CFATAL(fseek(_prv, 0, SEEK_CUR) < PRV_MAX_HEADER,
           "unexpectedly long header in prv file (accepts up to %d chars)",
           PRV_MAX_HEADER);
}

void
paratrace::TraceImpl::writePcf ()
{
    /** @todo Let the user override these default values */
    wpcf("DEFAULT_OPTIONS\n");
    wpcf("\n");
    wpcf("LEVEL\tTASK\n");
    wpcf("UNITS\tNANOSEC\n");
    wpcf("LOOK_BACK\t100\n");
    wpcf("SPEED\t1\n");
    wpcf("FLAG_ICONS\tENABLED\n");
    wpcf("NUM_OF_STATE_COLORS\t"FMT_size_t"\n", _states.size());
    wpcf("YMAX_SCALE\t18\n");
    wpcf("\n");
    wpcf("\n");
    wpcf("DEFAULT_SEMANTIC\n");
    wpcf("\n");
    wpcf("THREAD_FUNC\tState As Is\n");
    wpcf("\n");

    wpcf("STATES\n");
    for (StateRepo::const_iterator i = _states.begin();
         i != _states.end(); i++) {
        wpcf(FMT_state"\t%s\n", i->first, i->second.first.c_str());
    }
    wpcf("\n");
    wpcf("STATES_COLOR\n");
    for (StateRepo::const_iterator i = _states.begin();
         i != _states.end(); i++) {
        const Color & color = i->second.second;
        wpcf(FMT_state"\t{%d,%d,%d}\n", i->first, color.r, color.g, color.b);
    }
    wpcf("\n");

    wpcf("EVENT_TYPE\n");
    int idx = 0;
    for (EventRepo::const_iterator i = _events.begin();
         i != _events.end(); i++, idx++) {
        /** @todo Not using user-defined gradients */
        wpcf("%d\t"FMT_event" %s\n", idx, i->first, i->second.c_str());
    }
    wpcf("\n");

    /** @todo Add support for user-defined names for event values. */
}

void
paratrace::TraceImpl::writeRow ()
{
    _contexts.writeRow(_row);
}


paratrace::TraceImpl::~TraceImpl ()
{
    CFATAL(_pending_state.size() == 0,
        "there are pending state records");
    CFATAL(_pending_comm.size() == 0,
        "there are pending communication records");
    CFATAL(_pending_clock.size() == 0,
        "there are pending records");

    writePrvHeader();
    writePcf();
    writeRow();

    CSFATAL(!fclose(_prv), "closing .prv file");
    CSFATAL(!fclose(_pcf), "closing .pcf file");
    CSFATAL(!fclose(_row), "closing .row file");
}

void
paratrace::TraceImpl::setStart (const paratrace::clock time)
{
    _clock_start = time;
}

void
paratrace::TraceImpl::registerNode (const paratrace::cpu ncpus,
                                const std::string name)
{
    _resources.registerNode(ncpus, name);
}

//////////////////////////////////////////////////////////////////////
// Event interface

void
paratrace::TraceImpl::registerEvent (const paratrace::event event, const std::string name)
{
    CERROR(_events.find(event) == _events.end(),
           "cannot register a duplicate paraver event: %s - %s",
           _events.find(event)->second.c_str(),
           name.c_str());
    _events.insert(std::make_pair(event, name));
}

void
paratrace::TraceImpl::recordEvent (const paratrace::cpuid cpuid,
                                   const paratrace::Context context,
                                   const paratrace::clock time,
                                   const paratrace::event event,
                                   const paratrace::event_value value)
{
    Record::Event * record = new Record::Event(cpuid, context,
                                               time - _clock_start,
                                               event, value);
    _pending_clock.insert(PendingClockRepo::value_type(&record->getIndex(),
                                                       record));
}

//////////////////////////////////////////////////////////////////////
// State interface

void
paratrace::TraceImpl::registerState (const paratrace::state state, const std::string name,
                                     const paratrace::Color color)
{
    CERROR(_states.find(state) == _states.end(),
           "cannot register a duplicate paraver state: %s - %s",
           _states.find(state)->second.first.c_str(),
           name.c_str());
    _states.insert(std::make_pair(state, std::make_pair(name, color)));
}

void
paratrace::TraceImpl::recordStateBegin (const paratrace::cpuid cpuid,
                                        const paratrace::Context context,
                                        const paratrace::clock time,
                                        const paratrace::state state)
{
    // only state records set context location as other records lie inside them
    const paratrace::node nodeid = _resources.getNode(cpuid);
    _contexts.setLocation(context, nodeid);

    Record::State * record = new Record::State(cpuid, context,
                                               time - _clock_start,
                                               state);
    std::pair<PendingStateRepo::iterator, bool> res =
        _pending_state.insert(PendingStateRepo::value_type(&record->getIndex().context,
                                                           record));
    CFATAL(res.second,
           "cannot start a state record for a context having an unfinished state"
           " ("FMT_app":"FMT_task":"FMT_thread" @ "FMT_clock")",
           context.app, context.task, context.thread,
           (*res.first).second->getBegin());

    // this call is necessary for correctness, but the result only used for
    // debugging
    std::pair<PendingClockRepo::iterator, bool> tmp __attribute__((unused)) =
        _pending_clock.insert(PendingClockRepo::value_type(&record->getIndex(),
                                                           record));
    ASSERT(tmp.second);
}

void
paratrace::TraceImpl::recordStateEnd (const paratrace::Context context,
                                      const paratrace::clock time)
{
    const paratrace::clock delta = time - _clock_start;

    // lookup pending record
    PendingStateRepo::iterator it = _pending_state.find(&context);
    Record * record = it->second;
    CFATAL(record,
           "could not find pending state to finish"
           " ("FMT_app":"FMT_task":"FMT_thread")",
           context.app, context.task, context.thread);
    Record::State * state = (Record::State *)record;

    // delete from record-specific pending list
    _pending_state.erase(it);
    // mark as finished
    state->end(delta);
    // State is the only record-type that sets `stop_clock' as all other records
    // lie inside a state
    _clock_stop = delta > _clock_stop ? delta : _clock_stop;
    // sweep finished records waiting for in-order retirement
    retirePending();
}

//////////////////////////////////////////////////////////////////////
// Communication interface

inline
paratrace::PendingCommRepo::iterator
paratrace::TraceImpl::recordCommGet (const paratrace::msg_id id)
{
    PendingCommRepo::iterator it = _pending_comm.find(id);
    Record * record = it->second;
    CFATAL(record, "could not find pending communication record ("FMT_msg_id")", id);
    return it;
}

void
paratrace::TraceImpl::recordCommLogBegin (const paratrace::msg_id id,
                                          const paratrace::msg_size size,
                                          const paratrace::cpuid cpuid,
                                          const paratrace::Context context,
                                          const paratrace::clock time)
{
    Record::Comm * record = new Record::Comm(id, size, cpuid, context,
                                             time - _clock_start);
    std::pair<PendingCommRepo::iterator, bool> res =
        _pending_comm.insert(PendingCommRepo::value_type(id, record));
    CFATAL(res.second, "cannot start a communication record with"
           " a message id pending for being finished ("FMT_msg_id")", id);

    _pending_clock.insert(PendingClockRepo::value_type(&record->getIndex(), record));
}

void
paratrace::TraceImpl::recordCommPhysBegin (const paratrace::msg_id id,
                                           const paratrace::clock time)
{
    Record::Comm * record = (Record::Comm *)recordCommGet(id)->second;
    record->physBegin(time - _clock_start);
}

void
paratrace::TraceImpl::recordCommPhysEnd (const paratrace::msg_id id,
                                         const paratrace::clock time)
{
    Record::Comm * record = (Record::Comm *)recordCommGet(id)->second;
    record->physEnd(time - _clock_start);
}

void
paratrace::TraceImpl::recordCommLogEnd (const paratrace::msg_id id,
                                        const paratrace::cpuid cpuid,
                                        const paratrace::Context context,
                                        const paratrace::clock time)
{
    PendingCommRepo::iterator it = recordCommGet(id);
    Record::Comm * record = (Record::Comm *)it->second;

    _pending_comm.erase(it);         // delete from record-specific pending list
    record->logEnd(cpuid, context, time - _clock_start); // mark as finished
    /* NOTE: retiring only in stateEnd would suffice, but in a system with many
     * communication records in the same state, it could eat up a lot of memory.
     */
    retirePending();
}

//////////////////////////////////////////////////////////////////////
// Generic record internal API

inline
void
paratrace::TraceImpl::retirePending ()
{
    for (PendingClockRepo::iterator it = _pending_clock.begin();
         it != _pending_clock.end(); it = _pending_clock.begin()) {
        Record * record = it->second;
        if (!record->isEnded()) {
            return;
        }
        retire(it);
    }
}

inline
void
paratrace::TraceImpl::retire (PendingClockRepo::iterator it)
{
    Record * record = it->second;
    _pending_clock.erase(it);
    record->write(_prv);
    delete record;
}
