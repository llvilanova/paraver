/* -*- mode: c++ -*-
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "paratrace/ContextRepo/Thread.h"
#include "paratrace/ContextRepo/Task.h"
#include "paratrace/ContextRepo/App.h"


inline
void
paratrace::ContextRepo::registerApp (const Context context,
                                     const std::string name)
{
    getApp(context.app).setName(name);
}

inline
void
paratrace::ContextRepo::registerTask (const Context context,
                                      const std::string name)
{
    getApp(context.app).registerTask(context, name);
}

inline
void
paratrace::ContextRepo::registerThread (const Context context,
                                        const std::string name)
{
    getApp(context.app).getTask(context.task).registerThread(context, name);
}

inline
void
paratrace::ContextRepo::setLocation (const Context context,
                                     const paratrace::node node)
{
    getApp(context.app).setLocation(context, node);
}

inline
paratrace::ContextRepo::App &
paratrace::ContextRepo::getApp (const paratrace::app app)
{
    App * res;
    if (app >= _apps.size()) {
        _apps.resize(app+1, NULL);
        res = new App();
        _apps[app] = res;
    }
    else if (_apps[app] == NULL) {
        res = new App();
        _apps[app] = res;
    }
    else {
        res = _apps[app];
    }
    return *res;
}
