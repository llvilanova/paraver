/* -*- mode: c++ -*-
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef PARATRACE_RECORD_H
#define PARATRACE_RECORD_H

#include <cstdio>

#include <paratrace.h>
#include "paratrace/RecordIndex.h"


namespace paratrace {

/** Abstract representation of an in-flight paraver record. */
class PRT_LOCAL Record
{
public:
    Record (const int type, const paratrace::Context context,
            const paratrace::clock time);

    const RecordIndex & getIndex () const;
    const paratrace::clock getBegin () const;

    virtual const bool isEnded () const = 0;
    virtual void write (FILE * file) const = 0;

    class Event;
    class State;
    class Comm;

protected:
    const RecordIndex _index;
};

};

#include "paratrace/Record.ipp"

#endif  // PARATRACE_RECORD_H
