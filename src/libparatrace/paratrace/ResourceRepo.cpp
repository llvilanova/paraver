/*
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "paratrace/ResourceRepo.h"


void
paratrace::ResourceRepo::registerNode (const paratrace::cpu ncpus,
                                       const std::string name)
{
    paratrace::node nodeid = _nodes.size() + 1;
    paratrace::cpuid cpuid = _cpuids.size();
    CFATAL(cpuid < cpuid + ncpus, "I ran out of CPU IDs");

    Node * node = new Node(nodeid, name, ncpus, cpuid);
    _nodes.push_back(node);

    _cpuids.resize(cpuid + ncpus, nodeid);
}

void
paratrace::ResourceRepo::writePrvHeader (FILE * file) const
{
    fprintf(file, ":"FMT_size_t"(", _nodes.size());
    for (NodeList::const_iterator i = _nodes.begin();
         i != _nodes.end(); i++) {
        fprintf(file, FMT_cpu",", (*i)->ncpus);
    }
    fseek(file, -1, SEEK_CUR);
    fprintf(file, ")");
}

paratrace::ResourceRepo::~ResourceRepo ()
{
    for (NodeList::iterator it = _nodes.begin();
         it != _nodes.end(); it++) {
        delete *it;
    }

}
