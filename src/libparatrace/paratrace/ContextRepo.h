/* -*- mode: c++ -*-
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#ifndef PARATRACE_CONTEXTREPO_H
#define PARATRACE_CONTEXTREPO_H

#include <vector>
#include <string>

#include <paratrace.h>


namespace paratrace {

class PRT_LOCAL ContextRepo
{
public:
    void registerApp (const Context context, const std::string name);
    void registerTask (const Context context, const std::string name);
    void registerThread (const Context context, const std::string name);

    /**
     * Set the current physical location of the given Context.
     *
     * Implicitly creates all the app, task, thread hierarchy if it did not
     * exist.
     *
     * This must be called, at least, once for each node the context's task
     * runs on.
     */
    void setLocation (const Context context, const paratrace::node node);

    void writePrvHeader (FILE *file) const;
    void writeRow (FILE *file) const;

    ~ContextRepo ();

private:
    class App;
    class Task;
    class Thread;

    typedef std::vector<App *> AppRepo;
    AppRepo _apps;

    App & getApp (const paratrace::app app);
};

};

#include "ContextRepo.ipp"

#endif  // PARATRACE_CONTEXTREPO_H
