/* -*- mode: c++ -*-
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#ifndef PARATRACE_RESOURCEREPO_H
#define PARATRACE_RESOURCEREPO_H

#include <vector>
#include <string>

#include <paratrace.h>


namespace paratrace {

/** Store information on available physical resources. */
class PRT_LOCAL ResourceRepo
{
public:

    /**
     * Register information for a new node.
     *
     * Nodes must be added in order, so their index is implicitly assigned.
     *
     * @todo Empty resource definition still not supported.
     *
     * @param ncpus Number of CPUs in the node.
     * @param name  Name of the node.
     *              If name is empty, will use "NODE<number>" as name.
     */
    void registerNode (const paratrace::cpu ncpus,
                       const std::string name = "");

    /** Get paraver's system global CPU ID. */
    const paratrace::cpuid getCpuid (const paratrace::node node,
                                     const paratrace::cpu cpu) const;
    /** Get paraver's system global Node ID. */
    const paratrace::node getNode (const paratrace::cpuid cpuid) const;

    void writePrvHeader (FILE *file) const;

    ~ResourceRepo ();

private:
    class Node;

    typedef std::vector<Node *> NodeList;
    NodeList _nodes;

    typedef std::vector<paratrace::node> CpuidList;
    CpuidList _cpuids;
};

};

#include "ResourceRepo.ipp"

#endif  // PARATRACE_RESOURCEREPO_H
