/* -*- mode: c++ -*-
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "formats.h"
#define CHECKPKG "libparatrace"
#include "checks.h"

#include "paratrace/ResourceRepo/Node.h"


inline
const paratrace::cpuid
paratrace::ResourceRepo::getCpuid (const paratrace::node node,
                                   const paratrace::cpu cpu) const
{
    CFATAL(node < _nodes.size(),
           "cannot access unexistent node ("FMT_node")", node);
    Node * n = _nodes[node];
    CFATAL(cpu < n->ncpus,
           "cannot access unexistent CPU ("FMT_node":"FMT_cpu")", node, cpu);
    return n->first + cpu;
}

inline
const paratrace::node
paratrace::ResourceRepo::getNode (const paratrace::cpuid cpuid) const
{
    ASSERT(cpuid < _cpuids.size(),
           "CPU ID '"FMT_cpuid"' out of bounds", cpuid);
    return _cpuids[cpuid];
}
