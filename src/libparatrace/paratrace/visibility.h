/*
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef PARATRACE_VISIBILITY_H
#define PARATRACE_VISIBILITY_H

/* Generic helper definitions for shared library support */
#if defined _WIN32 || defined __CYGWIN__
#  define PRT_HELPER_DLL_IMPORT __declspec(dllimport)
#  define PRT_HELPER_DLL_EXPORT __declspec(dllexport)
#  define PRT_HELPER_DLL_LOCAL
#else
#  if __GNUC__ >= 4
#    define PRT_HELPER_DLL_IMPORT __attribute__ ((visibility("default")))
#    define PRT_HELPER_DLL_EXPORT __attribute__ ((visibility("default")))
#    define PRT_HELPER_DLL_LOCAL  __attribute__ ((visibility("hidden")))
#  else
#    define PRT_HELPER_DLL_IMPORT
#    define PRT_HELPER_DLL_EXPORT
#    define PRT_HELPER_DLL_LOCAL
#  endif
#endif

/* Now we use the generic helper definitions above to define PRT_API and
 * PRT_LOCAL.
 * PRT_API is used for the public API symbols. It either DLL imports or DLL
 * exports (or does nothing for static build).
 * PRT_LOCAL is used for non-api symbols.
 */

#if defined(PRT_DLL) && not defined(DEBUG)
/* compiled as a non-debug shared library */
#  ifdef PRT_DLL_EXPORTS           /* defined if we're building the library
                                    * instead of using it                     */
#    define PRT_API PRT_HELPER_DLL_EXPORT
#  else
#    define PRT_API PRT_HELPER_DLL_IMPORT
#  endif  /* PRT_DLL_EXPORTS */
#  define PRT_LOCAL PRT_HELPER_DLL_LOCAL
#else  /* PRT_DLL is not defined, meaning we're in a static library */
#  define PRT_API
#  define PRT_LOCAL
#endif  /* PRT_DLL */

#endif  /* PARATRACE_VISIBILITY_H */
