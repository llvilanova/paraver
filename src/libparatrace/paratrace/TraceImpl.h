/* -*- mode: c++ -*-
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#ifndef PARATRACE_TRACEIMPL_H
#define PARATRACE_TRACEIMPL_H

#include <string>
#include <memory>
#include <map>
#include <functional>

#include <paratrace.h>


#include "paratrace/Record/State.h"
#include "paratrace/RecordIndex.h"

namespace paratrace {

    class Record;

    typedef std::map<const RecordIndex *, Record *,
                     RecordIndex> PendingClockRepo;
    typedef std::map<const Context *, Record::State *> PendingStateRepo;
    typedef std::map<const paratrace::msg_id, Record *> PendingCommRepo;
    typedef std::map<const paratrace::event, const std::string> EventRepo;
    typedef std::map< const paratrace::state,
                      const std::pair<const std::string, const Color>
                      > StateRepo;

    class ContextRepo;
    class ResourceRepo;

};

namespace std {

template<>
struct PRT_LOCAL less<const paratrace::Context *>
{
    inline
    bool operator() (const paratrace::Context *ctx1, const paratrace::Context *ctx2) const
    {
        return *ctx1 < *ctx2;
    }
};

}


#include "paratrace/ContextRepo.h"
#include "paratrace/ResourceRepo.h"


class PRT_LOCAL paratrace::TraceImpl
{
public:
    TraceImpl (const std::string path);
    ~TraceImpl ();

    void setStart (const paratrace::clock time);

    void registerNode (const paratrace::cpu ncpu, const std::string name = "");

    // Event record interface

    void registerEvent (const paratrace::event event, const std::string name);

    void recordEvent (const paratrace::cpuid cpuid,
                        const paratrace::Context context,
                        const paratrace::clock time,
                        const paratrace::event event,
                        const paratrace::event_value value);


    // State record interface

    void registerState (const paratrace::state state, const std::string name,
                        const Color color);

    void recordStateBegin (const paratrace::cpuid cpuid,
                           const paratrace::Context context,
                           const paratrace::clock time,
                           const paratrace::state state);

    void recordStateEnd (const paratrace::Context context,
                         const paratrace::clock time);


    // Communication record interface

    void recordCommLogBegin (const paratrace::msg_id id,
                             const paratrace::msg_size size,
                             const paratrace::cpuid cpuid,
                             const paratrace::Context context,
                             const paratrace::clock time);

    void recordCommPhysBegin (const paratrace::msg_id id,
                              const paratrace::clock time);

    void recordCommPhysEnd (const paratrace::msg_id id,
                            const paratrace::clock time);

    void recordCommLogEnd (const paratrace::msg_id id,
                           const paratrace::cpuid cpuid,
                           const paratrace::Context context,
                           const paratrace::clock time);

private:
    FILE *_prv, *_pcf, *_row;
    paratrace::clock _clock_start, _clock_stop;

    PendingClockRepo _pending_clock;
    PendingStateRepo _pending_state;
    PendingCommRepo  _pending_comm;

    EventRepo _events;
    StateRepo _states;
    ContextRepo _contexts;
    ResourceRepo _resources;

    PendingCommRepo::iterator recordCommGet (const paratrace::msg_id id);


    void retirePending ();
    void retire (PendingClockRepo::iterator it);

    void writePrvHeader ();
    void writePcf ();
    void writeRow ();

    // Make the class non-copyable.
    TraceImpl (const TraceImpl &);
    TraceImpl & operator= (const TraceImpl &);
};

#endif  // PARATRACE_TRACEIMPL_H
