/* -*- mode: c++ -*-
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#ifndef PARATRACE_TRACE_H
#define PARATRACE_TRACE_H

#include <string>
#include <memory>

#include <paratrace.h>


/**
 * @mainpage libparatrace: Library for generating paraver traces
 *
 * You can get more information on paraver from
 * <a href="http://www.bsc.es/plantillaA.php?cat_id=485">here</a>.
 *
 * The core functionality to write traces to be read by \e paraver can be found
 * in paratrace::Trace.
 */

namespace paratrace {

class PRT_LOCAL TraceImpl;

/**
 * Strict low-level API to write paraver traces.
 *
 * @note This class is \b not reentrant. In order to efficiently support tracing
 * multiple concurrent threads you should use \em parabin::Trace (found in \em
 * libparabin).
 *
 * @warning Records must begin in order.
 */
class PRT_API Trace
{
public:
    /**
     * Create a new trace stream.
     *
     * @param path  Path to the output <tt>.prv</tt> file.
     *
     * If path does not end with a <tt>.prv</tt> suffix, it will be
     * automatically added.
     *
     * Any other paraver-specific output files (e.g., <tt>.pcf</tt>) will be
     * created by substituting the <tt>.prv</tt> suffix in @em path.
     */
    Trace (const std::string path);

    /** Finishes writing the traces to disk and destroys the object. */
    ~Trace ();

    /**
     * Set starting timestamp of trace.
     *
     * This will trim any initial time steps from the trace, while other methods
     * will still use absolute timestamps.
     *
     * @todo Paraver should be able to trim the initial timestamp shown,
     * withtout shifting timestamps (e.g., supported through the .pcf file)
     */
    void setStart (const paratrace::clock time);

    /** Register a new node. */
    void registerNode (const paratrace::cpu ncpus, const std::string name = "");


    // Event record interface

    /** Set the name for an event. */
    void registerEvent (const paratrace::event event, const std::string name);

    /** Record a new event. */
    void recordEvent (const paratrace::cpuid cpu,
                      const paratrace::Context context,
                      const paratrace::clock time,
                      const paratrace::event event,
                      const paratrace::event_value value);


    // State record interface

    /** Establish how to depict a state. */
    void registerState (const paratrace::state state, const std::string name,
                        const paratrace::Color color);

    /** Record the beginning of a new state. */
    void recordStateBegin (const paratrace::cpuid cpuid,
                           const paratrace::Context context,
                           const paratrace::clock time,
                           const paratrace::state state);

    /** Record the beginning of a new state. */
    void recordStateEnd (const paratrace::Context context,
                         const paratrace::clock time);


    // Communication record interface

    /** Record the logical beginning of a communication. */
    void recordCommLogBegin (const paratrace::msg_id id,
                             const paratrace::msg_size size,
                             const paratrace::cpuid cpuid,
                             const paratrace::Context context,
                             const paratrace::clock time);

    /** Record the physical beginning of a communication. */
    void recordCommPhysBegin (const paratrace::msg_id id,
                              const paratrace::clock time);

    /** Record the physical ending of a communication. */
    void recordCommPhysEnd (const paratrace::msg_id id,
                            const paratrace::clock time);

    /** Record the logical ending of a communication. */
    void recordCommLogEnd (const paratrace::msg_id id,
                           const paratrace::cpuid cpuid,
                           const paratrace::Context context,
                           const paratrace::clock time);

private:
    std::auto_ptr< TraceImpl > _pimpl;

    // Make the class non-copyable.
    Trace (const Trace &);
    Trace & operator= (const Trace &);
};

};

#endif  // PARATRACE_TRACE_H
