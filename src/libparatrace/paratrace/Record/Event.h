/* -*- mode: c++ -*-
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#ifndef PARATRACE_RECORD_EVENT_H
#define PARATRACE_RECORD_EVENT_H

#include "paratrace/Record.h"


class PRT_LOCAL paratrace::Record::Event : public paratrace::Record
{
public:
    Event (const paratrace::cpuid cpuid, const paratrace::Context context,
           const paratrace::clock time,
           const paratrace::event event, const paratrace::event_value value);

    const bool isEnded () const;
    void write (FILE * file) const;

private:
    const paratrace::cpuid _cpuid;
    const paratrace::event _event;
    const paratrace::event_value _value;
};

#include "paratrace/Record/Event.ipp"

#endif  // PARATRACE_RECORD_EVENT_H
