/* -*- mode: c++ -*-
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#ifndef PARATRACE_RECORD_COMM_H
#define PARATRACE_RECORD_COMM_H

#include "paratrace/Record.h"


class PRT_LOCAL paratrace::Record::Comm : public paratrace::Record
{
public:
    Comm (const paratrace::msg_id id, const paratrace::msg_size size,
          const paratrace::cpuid cpuid, const paratrace::Context context,
          const paratrace::clock time);

    const bool isEnded () const;
    void write (FILE * file) const;

    void physBegin (const paratrace::clock time);
    void physEnd (const paratrace::clock time);
    void logEnd (const paratrace::cpuid cpuid,
                 const paratrace::Context context,
                 const paratrace::clock time);

private:
    const paratrace::msg_id _id;
    const paratrace::msg_size _size;
    const paratrace::cpuid _cpuid_begin;

    paratrace::clock _phys_begin;

    paratrace::clock _phys_end;

    paratrace::cpuid _cpuid_end;
    paratrace::Context _context_end;
    paratrace::clock _log_end;
};

#include "paratrace/Record/Comm.ipp"

#endif  // PARATRACE_RECORD_COMM_H
