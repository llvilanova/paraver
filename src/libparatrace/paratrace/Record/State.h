/* -*- mode: c++ -*-
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#ifndef PARATRACE_RECORD_STATE_H
#define PARATRACE_RECORD_STATE_H

#include "paratrace/Record.h"


class PRT_LOCAL paratrace::Record::State : public paratrace::Record
{
public:
    State (const paratrace::cpuid cpuid, const paratrace::Context context,
           const paratrace::clock time, const paratrace::state state);

    const bool isEnded () const;
    void write (FILE * file) const;

    void end (const paratrace::clock time);

private:
    const paratrace::cpuid _cpuid;
    const paratrace::state _state;
    paratrace::clock _time;
};

#include "paratrace/Record/State.ipp"

#endif  // PARATRACE_RECORD_STATE_H
