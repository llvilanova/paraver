/*
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "paratrace/Record/Comm.h"

#define CHECKPKG "libparatrace"
#include "checks.h"
#include "formats.h"

const bool
paratrace::Record::Comm::isEnded () const
{
    return _log_end != 0;
}

void
paratrace::Record::Comm::write (FILE * file) const
{
    CFATAL(_index.type == 3);
    fprintf(file, "3"
            ":"FMT_cpuid":"FMT_app":"FMT_task":"FMT_thread":"FMT_clock":"FMT_clock
            ":"FMT_cpuid":"FMT_app":"FMT_task":"FMT_thread":"FMT_clock":"FMT_clock
            ":"FMT_msg_size":"FMT_msg_id"\n",
            _cpuid_begin,
            _index.context.app, _index.context.task, _index.context.thread,
            _index.time, _phys_begin,
            _cpuid_end, _context_end.app, _context_end.task,
            _context_end.thread, _log_end, _phys_end,
            _size, _id);
}
