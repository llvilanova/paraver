/* -*- mode: c++ -*-
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

inline
paratrace::Record::State::State (const paratrace::cpuid cpuid,
                                 const paratrace::Context context,
                                 const paratrace::clock time,
                                 const paratrace::state state)
    :Record(1, context, time)
    ,_cpuid(cpuid)
    ,_state(state)
    ,_time(0)
{
}

inline
void
paratrace::Record::State::end (const paratrace::clock time)
{
    _time = time;
}
