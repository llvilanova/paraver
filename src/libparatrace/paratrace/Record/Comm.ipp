/* -*- mode: c++ -*-
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#define CHECKPKG "libparatrace"
#include "checks.h"
#include "formats.h"

inline
paratrace::Record::Comm::Comm (const paratrace::msg_id id,
                               const paratrace::msg_size size,
                               const paratrace::cpuid cpuid,
                               const paratrace::Context context,
                               const paratrace::clock time)
    :Record(3, context, time)
    ,_id(id)
    ,_size(size)
    ,_cpuid_begin(cpuid)
    ,_phys_begin(0)
    ,_phys_end(0)
    ,_cpuid_end(0)
    ,_context_end(Context())
    ,_log_end(0)
{
}

inline
void
paratrace::Record::Comm::physBegin (const paratrace::clock time)
{
    _phys_begin = time;
}

inline
void
paratrace::Record::Comm::physEnd (const paratrace::clock time)
{
    CFATAL(_phys_begin,
           "cannot end physical communication that has not still been started ("FMT_msg_id")",
           _id);
    _phys_end = time;
}

inline
void
paratrace::Record::Comm::logEnd (const paratrace::cpuid cpuid,
                                 const paratrace::Context context,
                                 const paratrace::clock time)
{
    CFATAL(_phys_end, "cannot end logical communication that has not still"
           " been physically ended ("FMT_msg_id")", _id);
    _cpuid_end = cpuid;
    _context_end = context;
    _log_end = time;
}
