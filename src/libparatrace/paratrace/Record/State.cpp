/*
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "paratrace/Record/State.h"

#define CHECKPKG "libparatrace"
#include "checks.h"
#include "formats.h"


const bool
paratrace::Record::State::isEnded () const
{
    return _time != 0;
}

void
paratrace::Record::State::write (FILE * file) const
{
    CFATAL(_index.type == 1);
    fprintf(file, "1:"FMT_cpuid":"FMT_app":"FMT_task":"FMT_thread":"FMT_clock":"
            FMT_clock":"FMT_state"\n",
            _cpuid,
            _index.context.app, _index.context.task, _index.context.thread,
            _index.time, _time, _state);
}
