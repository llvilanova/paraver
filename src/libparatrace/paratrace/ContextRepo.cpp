/*
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "paratrace/ContextRepo.h"

#include "formats.h"


void
paratrace::ContextRepo::writePrvHeader (FILE *file) const
{
    /* :nApps:<application>:<application>:... */
    fprintf(file, ":"FMT_size_t, _apps.size() - 1);
    unsigned int id = 1;
    for (AppRepo::const_iterator i = ++_apps.begin();
         i != _apps.end(); i++) {
        if (*i == NULL) {
            // skip invalid entries, will be checked later
            continue;
        }
        fprintf(file, ":");
        (*i)->writePrvHeader(file);
        id++;
    }
}

void
paratrace::ContextRepo::writeRow (FILE *file) const
{
    std::string s_apps = "";
    std::string s_tasks = "";
    std::string s_threads = "";

    paratrace::app n_apps = 0;
    paratrace::task n_tasks = 0;
    paratrace::thread n_threads = 0;

    AppRepo::const_iterator i = _apps.begin();
    CFATAL(_apps.size() > 1, "there are no traced applications");
    CFATAL(*i == NULL, "application IDs greater than zero required");

    i++;
    n_apps++;

    while (i != _apps.end()) {
        CFATAL(*i != NULL,
               "cannot have empty application IDs ("FMT_app")", n_apps);
        (*i)->writeRow(file, s_apps, n_apps, s_tasks, n_tasks, s_threads,
                       n_threads);
        i++;
        n_apps++;
    }

    fprintf(file, "LEVEL APPL   \tSIZE "FMT_app"\n%s\n",
            n_apps - 1, s_apps.c_str());
    fprintf(file, "LEVEL TASK   \tSIZE "FMT_task"\n%s\n",
            n_tasks, s_tasks.c_str());
    fprintf(file, "LEVEL THREADS\tSIZE "FMT_thread"\n%s\n",
            n_threads, s_threads.c_str());
}

paratrace::ContextRepo::~ContextRepo ()
{
    for (AppRepo::iterator it = _apps.begin();
         it != _apps.end(); it++) {
        delete *it;
    }
}
