/* -*- mode: c++ -*-
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef PARATRACE_RESOURCEREPO_NODE_H
#define PARATRACE_RESOURCEREPO_NODE_H

#include "paratrace/ResourceRepo.h"


struct paratrace::ResourceRepo::Node
{
    const std::string name;
    const paratrace::node node;
    const paratrace::cpu ncpus;
    const paratrace::cpuid first;

    Node (const paratrace::node node, const std::string name,
          const paratrace::cpu ncpus, const paratrace::cpuid start);
};

#endif  // PARATRACE_RESOURCEREPO_H
