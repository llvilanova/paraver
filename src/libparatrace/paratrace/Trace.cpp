/*
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <string>

#include "paratrace/Trace.h"
#include "paratrace/TraceImpl.h"


paratrace::Trace::Trace (const std::string path)
    :_pimpl(new TraceImpl(path))
{
}

paratrace::Trace::~Trace ()
{
}

void
paratrace::Trace::setStart (const paratrace::clock time)
{
    _pimpl->setStart(time);
}

void
paratrace::Trace::registerNode (const paratrace::cpu ncpus,
                                const std::string name)
{
    _pimpl->registerNode(ncpus, name);
}


void
paratrace::Trace::registerEvent (const paratrace::event event, const std::string name)
{
    _pimpl->registerEvent(event, name);
}

void
paratrace::Trace::recordEvent (const paratrace::cpuid cpuid,
                               const paratrace::Context context,
                               const paratrace::clock time,
                               const paratrace::event event,
                               const paratrace::event_value value)
{
    _pimpl->recordEvent(cpuid, context, time, event, value);
}


void
paratrace::Trace::registerState (const paratrace::state state,
                                 const std::string name,
                                 const paratrace::Color color)
{
    _pimpl->registerState(state, name, color);
}

void
paratrace::Trace::recordStateBegin (const paratrace::cpuid cpuid,
                                    const paratrace::Context context,
                                    const paratrace::clock time,
                                    const paratrace::state state)
{
    _pimpl->recordStateBegin(cpuid, context, time, state);
}

void
paratrace::Trace::recordStateEnd (const paratrace::Context context,
                                  const paratrace::clock time)
{
    _pimpl->recordStateEnd(context, time);
}


void
paratrace::Trace::recordCommLogBegin (const paratrace::msg_id id,
                                      const paratrace::msg_size size,
                                      const paratrace::cpuid cpuid,
                                      const paratrace::Context context,
                                      const paratrace::clock time)
{
    _pimpl->recordCommLogBegin(id, size, cpuid, context, time);
}

void
paratrace::Trace::recordCommPhysBegin (const paratrace::msg_id id,
                                       const paratrace::clock time)
{
    _pimpl->recordCommPhysBegin(id, time);
}

void
paratrace::Trace::recordCommPhysEnd (const paratrace::msg_id id,
                                     const paratrace::clock time)
{
    _pimpl->recordCommPhysEnd(id, time);
}

void
paratrace::Trace::recordCommLogEnd (const paratrace::msg_id id,
                                    const paratrace::cpuid cpuid,
                                    const paratrace::Context context,
                                    const paratrace::clock time)
{
    _pimpl->recordCommLogEnd(id, cpuid, context, time);
}
