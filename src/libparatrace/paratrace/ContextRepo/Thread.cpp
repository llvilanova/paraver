/*
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "paratrace/ContextRepo.h"

#include "formats.h"

static
const std::string
get_name (const std::string name, const paratrace::thread nthread)
{
    if (name != "") {
        return name;
    }
    else {
        char default_name[1024];        // it will never be larger than that
        sprintf(default_name, FMT_thread, nthread);
        return default_name;
    }
}

void
paratrace::ContextRepo::Thread::writeRow (FILE * file,
                                          const std::string task_name,
                                          const paratrace::thread n_thread,
                                          std::string & s_threads) const
{
    std::string thread_name = task_name + "." + get_name(_name, n_thread);
    s_threads += thread_name + "\n";
}
