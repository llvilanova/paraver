/*
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "paratrace/ContextRepo.h"

#include "formats.h"


void
paratrace::ContextRepo::App::writePrvHeader (FILE * file) const
{
    /* nTasks(<task>,<task>,...) */
    fprintf(file, FMT_size_t"(", _tasks.size() - 1);
    for (TaskRepo::const_iterator i = _tasks.begin();
         i != _tasks.end(); i++) {
        if (*i == NULL) {
            // skip invalid entries, will be checked later
            continue;
        }
        (*i)->writePrvHeader(file);
        fprintf(file, ",");
    }
    fseek(file, -1, SEEK_CUR);
    fprintf(file, ")");
}

static
const std::string
get_name (const std::string name, const paratrace::app napp)
{
    if (name != "") {
        return name;
    }
    else {
        char default_name[1024];        // it will never be larger than that
        sprintf(default_name, FMT_app, napp);
        return default_name;
    }
}

void
paratrace::ContextRepo::App::writeRow (FILE * file,
                                       std::string & s_apps,
                                       paratrace::app & n_apps,
                                       std::string & s_tasks,
                                       paratrace::task & n_tasks,
                                       std::string & s_threads,
                                       paratrace::thread & n_threads)
{
    std::string app_name = get_name(_name, n_apps);
    s_apps += app_name + "\n";

    TaskRepo::const_iterator i = _tasks.begin();
    CFATAL(_tasks.size() > 1, "no traced tasks for application '%s'",
           app_name.c_str());
    CFATAL(*i == NULL,
           "task IDs greater than zero required (in application '%s')",
           app_name.c_str());

    unsigned int n_task = 0;
    i++;
    n_task++;
    while (i != _tasks.end()) {
        CFATAL(*i != NULL, "cannot have empty task IDs (%s:%d)",
               app_name.c_str(), n_task);
        (*i)->writeRow(file, app_name, n_task, s_tasks, s_threads, n_threads);
        i++;
        n_task++;
    }
    n_tasks += n_task - 1;
}

paratrace::ContextRepo::App::~App ()
{
    for (TaskRepo::iterator i = _tasks.begin(); i != _tasks.end(); i++) {
        delete *i;
    }
}
