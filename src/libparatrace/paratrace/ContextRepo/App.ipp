/* -*- mode: c++ -*-
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

inline
void
paratrace::ContextRepo::App::setName (const std::string name)
{
    _name = name;
}

inline
void
paratrace::ContextRepo::App::setLocation (const Context context,
                                          const paratrace::node node)
{
    getTask(context.task).setLocation(context, node);
}

inline
void
paratrace::ContextRepo::App::registerTask (const Context context,
                                      const std::string name)
{
    getTask(context.task).setName(name);
}

inline
paratrace::ContextRepo::Task &
paratrace::ContextRepo::App::getTask (const paratrace::task task)
{
    Task * res;
    if (task >= _tasks.size()) {
        _tasks.resize(task+1, NULL);
        res = new Task();
        _tasks[task] = res;
    }
    else if (_tasks[task] == NULL) {
        res = new Task();
        _tasks[task] = res;
    }
    else {
        res = _tasks[task];
    }
    return *res;
}
