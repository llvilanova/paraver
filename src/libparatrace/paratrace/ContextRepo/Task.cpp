/*
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "paratrace/ContextRepo.h"

#include "formats.h"

void
paratrace::ContextRepo::Task::writePrvHeader (FILE * file) const
{
    /* nThreadsInTask:node */
    CFATAL(_node != 0, "task without having set a location");
    fprintf(file, FMT_size_t":"FMT_thread, _threads.size() - 1, _node);
}

static
const std::string
get_name (const std::string name, const paratrace::task ntask)
{
    if (name != "") {
        return name;
    }
    else {
        char default_name[1024];        // it will never be larger than that
        sprintf(default_name, FMT_task, ntask);
        return default_name;
    }
}

void
paratrace::ContextRepo::Task::writeRow (FILE * file,
                                        const std::string app_name,
                                        const paratrace::task n_task,
                                        std::string & s_tasks,
                                        std::string & s_threads,
                                        paratrace::thread & n_threads) const
{
    std::string task_name = app_name + "." + get_name(_name, n_task);
    s_tasks += task_name + "\n";

    ThreadRepo::const_iterator i = _threads.begin();
    CFATAL(_threads.size() > 1, "no traced tasks for task '%s:%s'",
           app_name.c_str(), task_name.c_str());
    CFATAL(*i == NULL,
           "thread IDs greater than zero required (in task '%s:%s')",
           app_name.c_str(), task_name.c_str());

    unsigned int n_thread = 0;
    i++;
    n_thread++;
    while (i != _threads.end()) {
        CFATAL(*i != NULL, "cannot have empty thread IDs (%s:%s:%d)",
               app_name.c_str(), task_name.c_str(), n_thread);
        (*i)->writeRow(file, task_name, n_thread, s_threads);
        i++;
        n_thread++;
    }
    n_threads += n_thread - 1;
}

paratrace::ContextRepo::Task::~Task ()
{
    for (ThreadRepo::iterator i = _threads.begin();
         i != _threads.end(); i++) {
        delete *i;
    }
}
