/* -*- mode: c++ -*-
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#define CHECKPKG "libparatrace"
#include "checks.h"


inline
void
paratrace::ContextRepo::Task::setName (const std::string name)
{
    _name = name;
}

inline
void
paratrace::ContextRepo::Task::setLocation (const Context context,
                                           const paratrace::node node)
{
    getThread(context.thread);
    if (_node != 0) {
        CFATAL(node == _node, "tasks cannot migrate between nodes");
    }
    else {
        _node = node;
    }
}

inline
void
paratrace::ContextRepo::Task::registerThread (const Context context,
                                              const std::string name)
{
    getThread(context.thread).setName(name);
}

inline
paratrace::ContextRepo::Thread &
paratrace::ContextRepo::Task::getThread (const paratrace::thread thread)
{
    Thread * res;
    if (thread >= _threads.size()) {
        _threads.resize(thread+1, NULL);
        res = new Thread();
        _threads[thread] = res;
    }
    else if (_threads[thread] == NULL) {
        res = new Thread();
        _threads[thread] = res;
    }
    else {
        res = _threads[thread];
    }
    return *res;
}

inline
paratrace::ContextRepo::Task::Task ()
    :_node(0)
{
}
