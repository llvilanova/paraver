/* -*- mode: c++ -*-
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef PARATRACE_CONTEXTREPO_APP_H
#define PARATRACE_CONTEXTREPO_APP_H

#include <vector>
#include <string>

#include "paratrace/ContextRepo.h"


class paratrace::ContextRepo::App
{
public:
    void setName (const std::string name);
    void setLocation (const Context context, const paratrace::node node);

    void registerTask (const Context context, const std::string name);
    Task & getTask (const paratrace::task task);

    void writePrvHeader (FILE * file) const;
    void writeRow (FILE * file, std::string & s_apps, paratrace::app & n_apps,
                   std::string & s_tasks, paratrace::task & n_tasks,
                   std::string & s_threads, paratrace::thread & n_threads);

    ~App ();

private:
    std::string _name;

    typedef std::vector<Task *> TaskRepo;
    TaskRepo _tasks;
};

#include "paratrace/ContextRepo/App.ipp"

#endif  // PARATRACE_CONTEXTREPO_APP_H
