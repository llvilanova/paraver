/* -*- mode: c++ -*-
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

inline
paratrace::RecordIndex::RecordIndex (const int type,
                                     const paratrace::Context context,
                                     const paratrace::clock time)
    :type(type)
    ,context(context)
    ,time(time)
{
}

inline
paratrace::RecordIndex::RecordIndex ()
    :type(0)
    ,context()
    ,time(0)
{
}

inline
bool
paratrace::RecordIndex::operator() (const paratrace::RecordIndex * idx1,
                                    const paratrace::RecordIndex * idx2) const
{
    // Record order resolution:
    //  (1) ascending time
    //  (2) descending record type
    //  (3) ascending context ID

    if (idx1->time < idx2->time)
        return true;
    else if (idx1->time > idx2->time)
        return false;

    if (idx1->type > idx2->type)
        return true;

    return idx1->context < idx2->context;
}
