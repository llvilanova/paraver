/* -*- mode: c++ -*-
 * Copyright (C) 2010, 2011 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#ifndef PARATRACE_H
#define PARATRACE_H

#include <stdint.h>

#include <paratrace/visibility.h>


/**
 * Trace writing abstractions and common type definitions.
 */
namespace paratrace
{
    typedef uint64_t clock;             /**< Paraver's timestamp.          */

    /**
     * Color description.
     */
    struct PRT_API Color
    {
        const uint8_t r,                /**< Red component.                */
            g,                          /**< Green component.              */
            b;                          /**< Blue component.               */

        // Some default colors
        static const Color black, gray, white,
            red, light_red,
            green, light_green,
            blue, light_blue;
    };

    typedef uint64_t node;              /**< System global node ID.        */
    typedef uint64_t cpu;               /**< Node-local CPU ID.            */
    typedef uint64_t cpuid;             /**< System global CPU ID.         */

    typedef uint64_t app;               /**< System-global application ID. */
    typedef uint64_t task;              /**< Application-local task ID.    */
    typedef uint64_t thread;            /**< Task-local thread ID.         */

    /**
     * Execution context ID.
     */
    struct PRT_API Context
    {

        paratrace::app app;             /**< Application ID.               */
        paratrace::task task;           /**< Task ID.                      */
        paratrace::thread thread;       /**< Thread ID.                    */

        /**
         * @pre All arguments must be greater than zero.
         */
        Context (const paratrace::app app, const paratrace::task task,
                 const paratrace::thread thread);
        /**
         * @post All context attributes are zero.
         */
        Context ();

        bool operator< (const Context & other) const;
        bool operator<= (const Context & other) const;
        bool operator> (const Context & other) const;
        bool operator>= (const Context & other) const;
        bool operator== (const Context & other) const;
        bool operator!= (const Context & other) const;
    };

    typedef uint64_t state;             /**< Context state ID.             */

    typedef uint64_t event;             /**< Event ID.                     */
    typedef uint64_t event_value;       /**< Event value.                  */

    typedef uint64_t msg_id;            /**< Unique message ID.            */
    typedef uint64_t msg_size;          /**< Message size.                 */
    typedef uint64_t msg_tag;           /**< Message tag.                  */
};


#include "paratrace.ipp"

#endif // PARATRACE_H
