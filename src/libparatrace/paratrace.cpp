/*
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <cstdio>

#define CHECKPKG "libparatrace"
#include "checks.h"

#include "paratrace.h"


const paratrace::Color
    paratrace::Color::black       = {  0,   0,   0},
    paratrace::Color::gray        = {127, 127, 127},
    paratrace::Color::red         = {255,   0,   0},
    paratrace::Color::light_red   = {255, 127, 127},
    paratrace::Color::green       = {  0, 255,   0},
    paratrace::Color::light_green = {127, 255, 127},
    paratrace::Color::blue        = {  0,   0, 255},
    paratrace::Color::light_blue  = {127, 127, 255},
    paratrace::Color::white       = {255, 255, 255};

paratrace::Context::Context (const paratrace::app app,
                             const paratrace::task task,
                             const paratrace::thread thread)
    :app(app)
    ,task(task)
    ,thread(thread)
{
    CERROR(app > 0, "paratrace::app must be greater than zero");
    CERROR(task > 0, "paratrace::task must be greater than zero");
    CERROR(thread > 0, "paratrace::thread must be greater than zero");
}

paratrace::Context::Context ()
    :app(0)
    ,task(0)
    ,thread(0)
{
}
