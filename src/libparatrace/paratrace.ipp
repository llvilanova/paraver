/* -*- mode: c++ -*-
 * Copyright (C) 2010, 2011 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This file is part of libparatrace.
 *
 * Libparatrace is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <stdio.h>

namespace paratrace {

inline
bool
Context::operator< (const Context & other) const
{
    if (this->app < other.app)
        return true;
    else if (this->app > other.app)
        return false;

    if (this->task < other.task)
        return true;
    else if (this->task > other.task)
        return false;

    if (this->thread < other.thread)
        return true;

    return false;
}

inline
bool
Context::operator<= (const Context & other) const
{
    return *this < other || *this == other;
}

inline
bool
Context::operator> (const Context & other) const
{
    return !(*this < other || *this == other);
}

inline
bool
Context::operator>= (const Context & other) const
{
    return *this > other || *this == other;
}

inline
bool
Context::operator== (const Context & other) const
{
    return this->app == other.app && this->task == this->task && this->thread == other.thread;
}

inline
bool
Context::operator!= (const Context & other) const
{
    return !(*this == other);
}

} // paratrace

// Local Variables:
// c-file-offsets: ((innamespace . 0))
// End:
