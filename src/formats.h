/*
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This header is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This header is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this header; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef FORMATS_H
#define FORMATS_H

#if __LP64__                            /* 64 bit */

#define FMT_size_t      "%lu"
#define FMT_clock       "%lu"
#define FMT_cpuid       "%lu"
#define FMT_node        "%lu"
#define FMT_cpu         "%lu"
#define FMT_app         "%lu"
#define FMT_task        "%lu"
#define FMT_thread      "%lu"
#define FMT_state       "%ld"
#define FMT_event       "%lu"
#define FMT_event_value "%lu"
#define FMT_msg_id      "%lu"
#define FMT_msg_size    "%lu"

#else

#define FMT_size_t      "%u"
#define FMT_clock       "%llu"
#define FMT_cpuid       "%llu"
#define FMT_node        "%llu"
#define FMT_cpu         "%llu"
#define FMT_app         "%llu"
#define FMT_task        "%llu"
#define FMT_thread      "%llu"
#define FMT_state       "%lld"
#define FMT_event       "%llu"
#define FMT_event_value "%llu"
#define FMT_msg_id      "%llu"
#define FMT_msg_size    "%llu"

#endif

#endif /* FORMATS_H */
