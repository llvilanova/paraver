/*
 * Copyright (C) 2010 Lluís Vilanova <vilanova@ac.upc.edu>
 *
 * This header is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This header is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this header; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef CHECKS_H
#define CHECKS_H

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef CHECKPKG
#error CHECKPKG must be defined before including this file
#endif

#if DEBUG

/** Assert a condition, otherwise FATAL
 *
 * For debugging purposes only.
 */
#define ASSERT(cond, args...)                                           \
    do {                                                                \
        if (!(cond)) {                                                  \
            FATAL(args);                                                \
        }                                                               \
    } while(0)

#else /* DEBUG */

#define ASSERT(cond, args...)

#endif /* DEBUG */

/** Print error message, aborting execution
 * An ERROR might be due to invalid user input.
 */
#define ERROR(msg, args...)                                             \
    do {                                                                \
        fprintf(stderr, CHECKPKG": "msg"\n", ##args);                   \
        fflush(stderr);                                                 \
        abort();                                                        \
    } while(0)

/** Conditional ERROR */
#define CERROR(cond, args...)                                           \
    do {                                                                \
        if (!(cond)) {                                                  \
            ERROR(args);                                                \
        }                                                               \
    } while (0)

/** Conditional ERROR for system calls (with \e strerror) */
#define CSERROR(cond, msg, args...)                                     \
    do {                                                                \
        if (!(cond)) {                                                  \
            ERROR(msg": %s", ##args, strerror(errno));                  \
        }                                                               \
    } while(0)

/** Print fatal message, aborting execution
 * A FATAL might be due to an unforeseen internal error.
 */
#define FATAL(msg, args...)                                             \
    do {                                                                \
        fprintf(stderr, CHECKPKG":%s:%d:%s: "msg"\n",                   \
                __FILE__, __LINE__, __func__, ##args);                  \
        fflush(stderr);                                                 \
        abort();                                                        \
    } while (0)

/** Conditional FATAL */
#define CFATAL(cond, args...)                                           \
    do {                                                                \
        if (!(cond)) {                                                  \
            FATAL(args);                                                \
        }                                                               \
    } while (0)

/** Conditional FATAL for system calls (with \e strerror) */
#define CSFATAL(cond, msg, args...)                    \
    do {                                               \
        if (!(cond)) {                                 \
            FATAL(msg": %s", ##args, strerror(errno)); \
        }                                              \
    } while(0)

#endif /* CHECKS_H */
